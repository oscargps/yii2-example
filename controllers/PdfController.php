<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use app\models\Pdf as PdfModel;
use yii\web\UploadedFile;
use mikehaertl\pdftk\Pdf;

class PdfController extends Controller
{  //hereda informacion de la clase controller

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionEncrypt()
    {
        $model = new PdfModel;
        if ($this->request->isPost) {
            $data = $this->request->post();
            $model->password = $data['password'];
            $this->loadFile($model);
        }
    }

    protected function loadFile(PdfModel $model)
    {

        define('MB', 1048576);

        $model->file = UploadedFile::getInstanceByName('file'); //obtengo el archivo
        if ($model->file) {
            $fileName = time() . '_' . $model->file->basename . '.' . $model->file->extension;
            $filePath = $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $fileName;
            $outputFile = $_SERVER['DOCUMENT_ROOT'] . '/encrypt/' . $fileName;
            if ($model->file->saveAs($filePath) && filesize($filePath) < 5 * MB) {
                // echo Encrypt($filePath, $model->password, $outputFile);
                if ($this->Encrypt($filePath, $model->password, $outputFile)) {
                    unlink($filePath);
                    echo 'encrypt/' . $fileName;
                } else {
                    echo "error";
                    unlink($filePath);
                }
            }
        }
    }

    function Encrypt($file, $password, $output)
    {
        $pdf = new Pdf($file);
        $result = $pdf->allow('AllFeatures')
            ->setPassword($password)          // Set owner password
            ->setUserPassword($password . '2')      // Set user password
            ->passwordEncryption(128)   // Set password encryption strength
            ->saveAs($output);
        return true;
        if ($result === false) {
            $error = $pdf->getError();
            echo $error;
            return false;
        }
    }
}
