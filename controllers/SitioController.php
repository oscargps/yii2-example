<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessController;
use yii\web\Controller;
use app\models\FormularioForm;


class SitioController extends Controller
{  //hereda informacion de la clase controller
    public function actionInicio()
    { //palabra action y despues el nombre del metodo
        $model = new FormularioForm;
        //verificar si hemos recibido datos del form y si se han validado los datos
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $valorRespuesta =  $model->valuea+$model->valueb;
            return $this->render('inicio', ['mensaje' => $valorRespuesta, 'model' => $model]); //retornar la vista
        }
        //render(vista,[params..])
        return $this->render('inicio', ['mensaje' => '', 'model' => $model]); //retornar la vista
    }
}
