<?php

namespace app\controllers;

use app\models\Book;
use app\models\BookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\web\UploadedFile;
use yii\data\Pagination;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'only' => ['index','view','update','delete'],
                    'rules' => [
                        // permitido para usuarios autenticados
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ]
                ],
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [ //simplemente carga la vista con el dato recibido
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book(); //instancia con la clase del modelo


        if ($this->request->isPost) {
            $this->loadImage($model);  //si es una solicitud, realiza el proceso de carga
        } else {
            $model->loadDefaultValues(); // si no carga valores por defecto
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); //encuentra el archivo

        if ($this->request->isPost) {
            if ($this->loadImage($model)) { //practicamente crea uno nuevo con el mismo id
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        //en caso de no ser una solicitud post, entonces muestra la vista de actializar con la informacion recibida
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id); //encuentra los datos

        if (file_exists($model->image)) { //si el archivo existe lo elimina
            unlink($model->image);
        }
        $model->delete(); //luego del archivo elimina la imagen
        return $this->redirect(['index']);
    }

    public function actionList()
    {
        $model = Book::find();

        $pagination = new Pagination([
            'defaultPageSize' => 4,
            'totalCount' => $model->count(),
        ]);

        $books = $model->orderBy('title')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('list', ['books' => $books, 'pagination' => $pagination]);
    }
    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }
        //lanza la excepcion si no encuentra el archivo
        throw new NotFoundHttpException('The requested page does not exist.');
    }


    protected function LoadImage(Book $model)
    {
        if ($model->load($this->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file'); //obtengo el archivo

            if ($model->validate()) { //valido el archivo, extension
                if ($model->file) {
                    if (file_exists($model->image)) { //si ya esxiste lo elimino
                        unlink($model->image);
                    }
                    $filePath = 'uploads/' . time() . '_' . $model->file->basename . '.' . $model->file->extension; //creo un nuevo nombre para el archivo
                    if ($model->file->saveAs($filePath)) {             //guardo el archivo
                        $model->image = $filePath;
                    }
                }
            }
            if ($model->save(false)) { //por ultimo guardo el resto de la informacion luego de guadar el archivo
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
    }


}
