<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

?>

<h1>Libro a la venta</h1>
<div class="row">
    <?php foreach ($books as $book) : ?>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <a href="#" class="thumbnail">

                <?= Html::img($book->image,['width' =>'100px']); ?>
                <?= Html::encode("{$book->title}") ?>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<?= LinkPager::widget(['pagination' => $pagination]) ?>