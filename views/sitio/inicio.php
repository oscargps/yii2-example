<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; //usado para mostrar formularios

if($mensaje){
    echo Html::tag('div',Html::encode($mensaje),['class'=>'alert alert-danger']);
}
 ?>
Hola usuario

<?php $formulario = ActiveForm::begin(); ?> 
<!-- //inicio el formulario -->

<!-- //defino las variables del formulario -->
<?= $formulario->field($model, 'valuea') ?> 
<?= $formulario->field($model, 'valueb') ?>

<!-- //añado el boton de submit -->
<div class="form-group">
    <?= HTML::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
<!-- //cierro el formulario -->

