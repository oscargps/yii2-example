<?php

namespace app\models;
use Yii;
use yii\base\Model;

class Pdf extends Model
{
    public $file;
    public $password;
    public function rules()
    { //reglas de los datos usados en el modelo
        return [
            [['password'], 'required'],
            [['password'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'pdf'],
        ];
    }

    // public function upload()
    // {
    //     if ($this->validate()) {
    //         $this->file->saveAs('uploads/' . $this->file->baseName . '.' . $this->file->extension);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
