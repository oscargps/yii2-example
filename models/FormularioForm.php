<?php

namespace app\models;
use yii\base\Model;

class FormularioForm extends Model
{
    public $valuea;
    public $valueb;

    public function rules() { //reglas de los datos usados en el modelo
        return [
            [['valuea','valueb'],'required'],
            ['valuea', 'number'],['valueb', 'number'],
        ];
    }

}
